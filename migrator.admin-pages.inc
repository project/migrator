<?php

/**
 * @file
 * Migrator remote system settings and item mappings.
 */

/**
 * System settings form for remote system.
 */
function migrator_settings_system() {
  $form = array();
  $options = array();
  $systems = glob(drupal_get_path('module', 'migrator') .'/*', GLOB_ONLYDIR);
  foreach ($systems as $option) {
    if (file_exists($option .'/system.inc')) {
      $key = preg_replace('/[^a-zA-Z0-9]/', '', basename($option)); // obsolete?
      $options[$key] = drupal_ucfirst(basename($option));
    }
  }
  $form['system'] = array(
    '#type' => 'select',
    '#title' => t('Remote system'),
    '#options' => $options,
    '#default_value' => migrator_system(),
  );

  $options_features = array();
  foreach (migrator_system_features() as $feature => $item) {
    $options_features[$feature] = drupal_ucfirst($feature);
  }
  $form['features'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Data to migrate'),
    '#options' => $options_features,
    '#default_value' => variable_get('migrator_migrate_features', array()),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function migrator_settings_system_submit($form_id, $form_values) {
  if ($form_values['op'] == t('Save')) {
    variable_set('migrator_system', $form_values['system']);
    variable_set('migrator_migrate_features', $form_values['features']);
  }
}

/**
 * Settings form for remote system database connection.
 */
function migrator_settings_database() {
  $form = array();
  $db = parse_url(variable_get('migrator_db', ''));
  $form['db'] = array(
    '#type' => 'fieldset',
    '#title' => t('Database connection'),
  );
  $form['db']['scheme'] = array(
    '#type' => 'radios',
    '#title' => t('Database interface'),
    '#options' => array('mysql' => 'MySQL', 'mysqli' => 'MySQLi'),
    '#default_value' => $GLOBALS['db_type'], // was: $db['scheme'],
    '#disabled' => TRUE,
    '#description' => t('Due to the database abstraction design in Drupal 5 and 6, only the current default database interface is supported.'),
  );
  $form['db']['host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => !empty($db['host']) ? $db['host'] : 'localhost',
    '#required' => TRUE,
  );
  $form['db']['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Database'),
    '#default_value' => substr($db['path'], 1),
    '#required' => TRUE,
  );
  $form['db']['user'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => $db['user'],
    '#required' => TRUE,
  );
  $form['db']['pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => $db['pass'],
    '#required' => TRUE,
  );
  $form['db']['db_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Table prefix'),
    '#default_value' => variable_get('migrator_db_prefix', 'jos_'),
  );
  $form[] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function migrator_settings_database_submit($form_id, $form_values) {
  $url = $form_values['scheme'] .'://'. $form_values['user'] .':'. $form_values['pass'] .'@'. $form_values['host'] .'/'. $form_values['path'];
  variable_set('migrator_db', $url);
  variable_set('migrator_db_is_default', (is_array($GLOBALS['db_url']) ? $GLOBALS['db_url']['default'] == $url : $GLOBALS['db_url'] == $url));
  variable_set('migrator_db_prefix', $form_values['db_prefix']);
}

/**
 * Mapping form for remote system user roles.
 */
function migrator_settings_roles() {
  migrator_check_db();

  $remote_roles = migrator_get('roles');
  $drupal_roles = user_roles(TRUE);
  $mappings = migrator_map_get('role');

  $form = array();
  $form['object'] = array(
    '#type' => 'value',
    '#value' => 'role',
  );
  $form['#mapping_type'] = 'select';
  migrator_mapping_form($form, $remote_roles, $drupal_roles, $mappings);
  return $form;
}

function migrator_settings_roles_submit($form_id, $form_values) {
  migrator_map_set('role', $form_values['mappings'], TRUE);
}

/**
 * Mapping form for remote system user roles.
 */
function migrator_settings_users() {
  migrator_check_db();

  $mappings = migrator_map_get('user');
  $remote_users = array();
  $drupal_users = array();
  $ops = array();

  if (count($mappings)) {
    $where = migrator_key('user') .' IN ('. implode(',', array_fill(0, count($mappings), '%d')) .')';
    $remote_users = migrator_get('users', $where, array_keys($mappings));
    foreach ($mappings as $rid => $uid) {
      $remote_users[$rid] = $remote_users[$rid]['name'];
      $user = user_load(array('uid' => $uid));
      $drupal_users[$uid] = theme('username', $user);
      $ops[$rid] = l(t('Delete'), "admin/settings/migrator/delete/user/$rid");
    }
  }
  
  $form = array();
  $form['object'] = array(
    '#type' => 'value',
    '#value' => 'user',
  );

  $form['remote_user'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#maxlength' => 60,
    // #name and #parents required for drupal_render().
    '#name' => 'remote_user',
    '#parents' => array('remote_user'),
    // Attach autocomplete behaviour. (id required)
    '#autocomplete_path' => 'admin/settings/migrator/autocomplete/user',
    '#id' => 'remote-user',
  );
  $form['drupal_user'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#maxlength' => 60,
    // #name and #parents required for drupal_render().
    '#name' => 'drupal_user',
    '#parents' => array('drupal_user'),
    // Attach autocomplete behaviour. (id required)
    '#autocomplete_path' => 'user/autocomplete',
    '#id' => 'drupal-user',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  $remote_users[-1] = drupal_render($form['remote_user']);
  $drupal_users[-1] = drupal_render($form['drupal_user']);
  $mappings[-1] = -1;
  $ops[-1] = drupal_render($form['submit']);

  migrator_mapping_form($form, $remote_users, $drupal_users, $mappings, $ops);
  return $form;
}

function migrator_settings_users_submit($form_id, $form_values) {
  $remote_user = migrator_get('uid', $form_values['remote_user']);
  $drupal_user = user_load(array('name' => $form_values['drupal_user']));
  
  migrator_map_set('user', array($remote_user => $drupal_user->uid));
}

/**
 * Generate a remote/local item mapping form in a table.
 *
 * If $form['#mapping_type'] is set to 'select', this function assumes a
 * 1:1 relationship between remote and local objects. Practical examples:
 * - User roles: Each remote role needs to be assigned to a Drupal role.
 *
 * @param &$form
 *   A form structure, passed by reference.
 * @param $remote
 *   An associative array containing remote system objects (id => name).
 * @param $local
 *   An associative array containing Drupal system objects (id => name),
 *   or a callback function to invoke with the mapping value.
 * @param $mappings
 *   An associative array containing id mappings for the objects above.
 * @param $ops
 *   An associative array containing operations for each row (optional).
 *   If empty, Migrator automatically adds a save button to the form.
 *
 * @see migrator_settings_roles(), migrator_settings_users()
 */
function migrator_mapping_form(&$form, $remote, $local, $mappings, $ops = NULL) {
  $form['#theme'] = 'migrator_mapping_form';
  $i = 0;
  foreach ($remote as $rid => $name) {
    $form['rows'][$i][0][] = array(
      '#value' => $remote[$rid],
    );
    if ($form['#mapping_type'] == 'select') {
      $form['rows'][$i][1][$rid] = array(
        '#type' => 'select',
        '#options' => $local,
        '#default_value' => isset($mappings[$rid]) ? $mappings[$rid] : NULL,
        '#parents' => array('mappings', $rid),
      );
    }
    else {
      $form['rows'][$i][1][] = array(
        '#value' => $local[$mappings[$rid]],
      );
    }
    if (!is_null($ops)) {
      $form['rows'][$i][2][] = array(
        '#value' => isset($ops[$rid]) ? $ops[$rid] : '',
      );
    }
    $i++;
  }
  if (is_null($ops)) {
    $form[] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }
}

/**
 * Theme a mapping table containing configured remote/local item mappings.
 *
 * @see migrator_mapping_form()
 */
function theme_migrator_mapping_form(&$form) {
  $output = '';
  if (count($form['rows'])) {
    $header = array();
    $object = $form['object']['#value'];
    $header[] = t('Remote @object', array('@object' => $object));
    $header[] = t('Drupal @object', array('@object' => $object));
    if (isset($form['rows'][0][2])) {
      $header[] = t('Operations');
    }
    $rows = array();
    foreach ($form['rows'] as $i => $row) {
      if (is_int($i)) {
        $rows[] = array(
          drupal_render($form['rows'][$i][0]),
          drupal_render($form['rows'][$i][1]),
          isset($form['rows'][$i][2]) ? drupal_render($form['rows'][$i][2]) : NULL,
        );
      }
    }
    $output .= theme('table', $header, $rows);
  }
  $output .= drupal_render($form);
  return $output;
}

