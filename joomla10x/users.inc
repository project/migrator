<?php

/**
 * @file
 * Joomla 1.0.x user mapping support.
 */

/**
 * Retrieve users, optionally filtered by WHERE condition.
 *
 * Note: Since Joomla supports only one role per user, this query already
 * includes the user role. Other systems may need additional queries.
 *
 * @ingroup migrator_joomla10x
 */
function migrator_get_joomla10x_users($where = '', $args = array()) {
  $users = array();
  $sql = "SELECT u.id as uid, u.username as name, u.email as mail, u.password as pass, acl.group_id as rid, u.usertype as role, IF(u.block = 1,0,1) as status, UNIX_TIMESTAMP(u.registerDate) as created, UNIX_TIMESTAMP(u.lastvisitDate) as access FROM {users} u INNER JOIN {core_acl_aro_groups} acl ON acl.name = u.usertype";
  if (!empty($where)) {
    $sql .= ' WHERE '. $where;
  }
  $result = db_query($sql, $args);
  while ($user = db_fetch_array($result)) {
    $user['roles'] = array($user['rid'] => $user['role']);
    unset($user['rid'], $user['role']);
    $users[$user['uid']] = $user;
  }
  return $users;
}

/**
 * Retrieve a user id with a given username.
 *
 * @ingroup migrator_joomla10x
 */
function migrator_get_joomla10x_uid($name = '') {
  if (!empty($name)) {
    return db_result(db_query("SELECT u.id FROM {users} u WHERE u.username = '%s'", $name));
  }
  return FALSE;
}

/**
 * Retrieve a pipe delimited string of autocomplete suggestions.
 *
 * @ingroup migrator_joomla10x
 */
function migrator_get_joomla10x_autocomplete_user($string = '') {
  $matches = array();
  if ($string) {
    $result = db_query_range("SELECT id, username FROM {users} WHERE LOWER(username) LIKE LOWER('%s%%')", $string, 0, 10);
    while ($user = db_fetch_object($result)) {
      $matches[$user->username] = check_plain($user->username);
   }
  }
  return $matches;
}

