<?php

/**
 * @file
 * Joomla 1.0.x user role mapping support.
 */

/**
 * Retrieve all user roles in-use.
 *
 * Note: Joomla creates default user roles that may not be used at all. Those
 * need not to be migrated.
 *
 * @ingroup migrator_joomla10x
 */
function migrator_get_joomla10x_roles() {
  $roles = array();
  $result = db_query("SELECT acl.group_id, u.usertype FROM {users} u INNER JOIN {core_acl_aro_groups} acl ON acl.name = u.usertype GROUP BY u.usertype");
  while ($role = db_fetch_array($result)) {
    $roles[$role['group_id']] = $role['usertype'];
  }
  return $roles;
}

