<?php

/**
 * @file
 * Defines general system information about remote system.
 */

/**
 * @defgroup migrator_joomla10x Joomla! 1.0.x support.
 * @{
 */

/**
 * Return a list of available remote system features.
 */
function migrator_joomla10x_system() {
  return array(
    'database' => array(
      '#required' => TRUE,
    ),
    'roles' => array(
      '#required' => TRUE,
      '#dependencies' => array('user'),
    ),
    'users' => array(
      '#required' => TRUE,
      '#dependencies' => array('user'),
    ),
    'taxonomy' => array(
      '#dependencies' => array('taxonomy'),
    ),
  );
}

/**
 * Return a remote system object database key.
 */
function migrator_joomla10x_key($object) {
  switch ($object) {
    case 'user':
      return 'u.id';

    default:
      return 'id';
  }
}

/**
 * Return a list of remote system modules (components/extensions).
 * // unused 12/04/2008 sun
 *
 * @todo Implement similar array like in _system(), most importantly to
 *   support (Drupal) module dependencies.
 */
function migrator_get_joomla10x_modules() {
  $modules = array();
  $result = db_query("SELECT m.name, m.option FROM {components} m WHERE m.option != '' GROUP BY m.option ORDER BY m.option");
  while ($module = db_fetch_array($result)) {
    $modules[$module['option']] = $module['name'];
  }
  return $modules;
}

/**
 * Return a list of remote system content-types.
 */


/**
 * @} End of "defgroup migrator_joomla10x".
 */

