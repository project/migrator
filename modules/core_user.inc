<?php

/**
 * @file
 * Programmatic user-related Drupal core creation functions.
 */

/**
 * Create a Drupal user.
 *
 * @param array $user
 *   A user object array retrieved from the remote system.
 *
 * @todo Introduce migrator_system_info() or similar to define whether remote
 *   system stores passwords in plain-text. Or even better: migrator_value():
 *   Like migrator_key(), but to allow altering certain object values like
 *   passwords.
 */
function migrator_create_user($user) {
  static $mappings;
  $account = new stdClass;

  // Check whether a custom mapping exists for this user.
  if (!isset($mappings['users'])) {
    $mappings['users'] = migrator_map_get('user');
  }
  if (isset($mappings['users'][$user['uid']])) {
    $account->uid = $mappings['users'][$user['uid']];
  }
  else {
    $account->uid = NULL;
  }
  $remote_user = $user;
  unset($user['uid']);

  // Assign user roles.
  if (!isset($mappings['roles'])) {
    $mappings['roles'] = migrator_map_get('role');
  }
  foreach ($user['roles'] as $rid => $role) {
    if (isset($mappings['roles'][$rid])) {
      unset($user['roles'][$rid]);
      $user['roles'][$mappings['roles'][$rid]] = NULL;
    }
    else {
      unset($user['roles'][$rid]);
    }
  }
  
  // Create the user.
  $drupal_user = user_save($account, $user);
  // Fix password.
  db_query("UPDATE {users} u SET u.pass = '%s' WHERE u.uid = %d", $user['pass'], $drupal_user->uid);

  // Write new user mapping for unmapped users.
  if (!$account->uid) {
    migrator_map_add('user', $remote_user['uid'], $drupal_user->uid);
  }
}

