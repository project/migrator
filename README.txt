
-- SUMMARY --

Migrator aims to become a generic migration module to convert an existing site
to Drupal.  It has been developed to support various other/external/remote
systems, like Joomla!, Wordpress, aso. through modularized/out-sourced data
retrieval functions.  It allows to map existing user roles and users, so that
a Drupal site can be pre-built and, for example, all contents of the remote
system user 'administrator' (with uid 123) can be mapped to the existing Drupal
user 'admin' (with uid 1).  All non-mapped objects will get new object ids upon
migration.

Currently, support for Joomla! 1.0.x is on the way.  Optional support for
additional remote system modules (or in other terminologies like Joomla!:
Components) is planned.

For a full description visit the project page:
  http://drupal.org/project/migrator
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/migrator


-- REQUIREMENTS --

* Demo module <http://drupal.org/project/demo>


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* Enable the module in administer >> Modules.


-- CONFIGURATION --

* Configure user permissions in administer >> User management >> Access control
  >> Migrator.

* Customize the remote system settings and object mappings in administer
  >> Site configuration >> Migrator.


-- USAGE --

* @todo Step-by-step guide for basic system migration.


-- CUSTOMIZATION --

* @todo Developer info about adding support for other remote systems and modules.
  

-- CONTACT --

Current maintainers:
* Daniel F. Kudwien (sun) - dev@unleashedmind.com

This project seeks co-maintainers all of the time.  There are many other CMS
that could be supported.  Please leave me a message via
http://drupal.org/user/54136/contact if you want to contribute to this project.

This project has been sponsored by:
* UNLEASHED MIND
  Specialized in consulting and planning of Drupal powered sites, UNLEASHED
  MIND offers installation, development, theming, customization, and hosting
  to get you started. Visit http://www.unleashedmind.com for more information.

